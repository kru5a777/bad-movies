import React from 'react';

class Filters extends React.Component {

    state = {
        type: 'movie',
        release_date: null
    };

    detectChange(e) {
        let params = this.state;
        params[e.target.name] = e.target.value;
        this.setState(params);
        this.props.updateList(params);
    }

    render() {
        return(
            <div className="filters">
                <select onChange={e => this.detectChange(e)} name="type" id="category">
                    <option value="movie">Фильмы</option>
                    <option value="tv">Сериалы</option>
                </select>
                <select onChange={e => this.detectChange(e)} name="release_date" id="years">
                    <option value="2020">2020</option>
                    <option value="1984">1984</option>
                    <option value="2012">2012</option>
                    <option value="1995">1995</option>
                </select>
            </div>
        );
    }
}

export default Filters;