import React from "react";
import { Link } from 'react-router-dom';

import MoviePoster from "../movie-item/movie-poster/MoviePoster";
import MovieTitle from "../movie-item/movie-title/MovieTitle";
import MovieReleaseDate from "../movie-item/movie-release-date/MovieReleaseDate";
import MovieDescription from "../movie-item/movie-description/MovieDescription";
import Filters from './filters/Filters';
import Loading from "../loading/Loading";
import './MovieList.scss';

const userAPIKey = '0cdf9d3a331415e4663971d7a52805c1';

class MovieList extends React.Component {

    state = {
        dataQueryString: `https://api.themoviedb.org/3/discover/[type]?sort_by=vote_average.asc&primary_release_year=[release_date]&api_key=${userAPIKey}`,
        movieItems: []
    };

    componentDidMount() {
        this.updateMovieList({type: 'movie', release_date: null});
    }

    updateMovieList = (filters) => {
        let { dataQueryString } = this.state;
        for (let key in filters) dataQueryString = dataQueryString.replace(`[${key}]`, filters[key]);
        const updatedMovieList = this.getBadMoviesList(dataQueryString);
        updatedMovieList.then(e => this.setState({movieItems: e}));
    };

    async getBadMoviesList(url) {
        const fetchedData = await fetch(url);
        const items = await fetchedData.json();
        return items['results'].slice(0, 10);
    }

    render() {
        const { movieItems } = this.state;
        if (typeof movieItems == "undefined" || movieItems.length < 1) {
            return (
                <Loading />
            );
        }
        return (
            <div>
                <Filters updateList={this.updateMovieList}/>
                {movieItems.map(item => {
                    return(
                        <div key={item.id} className='container'>
                             <Link key={item.id} to={`/more-about-movie/${item.id}`}>
                                 <div className='movie-item'>
                                     <MoviePoster poster={item['poster_path'] || item['backdrop_path']}/>
                                     <div className='movie-info'>
                                         <MovieTitle title={item['original_title'] || item['original_name']}/>
                                         <MovieReleaseDate date={item['release_date'] || item['first_air_date']}/>
                                         <MovieDescription description={item['overview']}/>
                                     </div>
                                 </div>
                            </Link>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default MovieList;