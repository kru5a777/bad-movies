import React from "react";
import Loading from "../loading/Loading";
import Backdrops from "../movie-item/backdrops/Backdrops";
import MovieCast from "../movie-item/movie-cast/MovieCast";
import MovieCrew from "../movie-item/movie-crew/MovieCrew";

const userAPIKey = '0cdf9d3a331415e4663971d7a52805c1';

class MovieItemMore extends React.Component {

    state = {
        loading: true,
        movieItem: null,
        credits: null
    };

    componentDidMount() {
        const { needId } = this.props;
        this.fetchItem(needId).then(r => r);
    }

    async fetchItem(id) {
        const fetchItemImages = await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${userAPIKey}&language=en-US&append_to_response=images&include_image_language=en,null`);
        const fetchMovieCredits = await fetch(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=${userAPIKey}`);
        const credits = await fetchMovieCredits.json();
        const moreInfo = await fetchItemImages.json();
        this.setState(
            {
                loading: false,
                movieItem: moreInfo,
                credits: credits
            }
        );
    };

    render() {
        if (this.state.loading === true || this.state.movieItem === null) {
            return (
                <Loading />
            )
        }
        return (
            <div className='container'>
                <div className='movie-more-container'>
                    <div className='movie-poster-more'>
                        <img src={'https://image.tmdb.org/t/p/original' + this.state.movieItem['poster_path']} alt=""/>
                    </div>
                    <div className='movie-info'>
                        <div className='movie-title-more'>
                            <h2>{this.state.movieItem['original_title']}</h2>
                        </div>
                        <div className='overview'>
                            <p>Overview:</p>
                            <p>{this.state.movieItem['overview']}</p>
                        </div>
                    </div>
                </div>
                <MovieCrew crew={this.state.credits.crew}/>
                <MovieCast cast={this.state.credits.cast}/>
                <Backdrops backdrops={this.state.movieItem.images.backdrops} />
            </div>
        );
    }
}

export default MovieItemMore;