import React from "react";
import './Loading.scss';

class Loading extends React.Component {
    render() {
        return(
            <div className='loading'>
                Loading...
            </div>
        );
    }
}

export default Loading;