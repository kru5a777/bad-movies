import React from "react";

class MovieDescription extends React.Component {
    render() {
        return(
            <p>{this.props.description}</p>
        );
    }
}

export default MovieDescription;