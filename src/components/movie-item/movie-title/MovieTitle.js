import React from "react";

class MovieTitle extends React.Component {
    render() {
        return(
            <h3>{this.props.title}</h3>
        );
    }
}

export default MovieTitle;