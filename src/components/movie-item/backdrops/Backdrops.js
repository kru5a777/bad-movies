import React from "react";
import './Backdrops.scss'

class Backdrops extends React.Component {

    render() {
        return(
            <div>
                <h3>Backdrops:</h3>
                <div className='backdrops'>
                    {this.props.backdrops.map((item) => {
                        return <img src={'https://image.tmdb.org/t/p/original' + item.file_path} alt=""/>
                    })}
                </div>
            </div>
        )
    }
}

export default Backdrops;