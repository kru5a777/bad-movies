import React from "react";
import './MovieCrew.scss'

class MovieCrew extends React.Component {
    render() {
        return (
            <div>
                <h3>Crew:</h3>
                <div className='crew-container'>
                    {this.props.crew.map(item => {
                        if (item.profile_path !== null) {
                            return(
                                <div>
                                    <div>
                                        <img src={'https://image.tmdb.org/t/p/original' + item.profile_path} alt=""/>
                                    </div>
                                    <div>
                                        <div>Name: {item.name}</div>
                                        <div>Job: {item.job}</div>
                                    </div>
                                </div>
                            );
                        }
                        return '';
                    })}
                </div>
            </div>
        );
    }
}

export default MovieCrew;