import React from "react";

class MovieReleaseDate extends React.Component {
    render() {
        return(
            <p>{this.props.date}</p>
        );
    }
}

export default MovieReleaseDate;