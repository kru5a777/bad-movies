import React from "react";
import './MovieCast.scss';

class MovieCast extends React.Component {
    render() {
        return(
            <div className='movie-cast'>
                <h3>Cast:</h3>
                <div className='cast-container'>
                    {this.props.cast.map(item => {
                        if (item.profile_path !== null) {
                            return(
                                <div>
                                    <div>
                                        <img src={'https://image.tmdb.org/t/p/original' + item.profile_path} alt=""/>
                                    </div>
                                    <div>
                                        <div>Name: {item.name}</div>
                                        <div>Starring: {item.character}</div>
                                    </div>
                                </div>
                            );
                        }
                        return '';
                    })}
                </div>
            </div>
        );
    }
}

export default MovieCast;