import React from "react";
import './MoviePoster.scss';

class MoviePoster extends React.Component {
    render() {
        return(
            <div className='movie-poster'>
                {this.props.poster !== null
                    ? <img src={'https://image.tmdb.org/t/p/original' + this.props.poster} alt="" />
                    : <img src={'http://placehold.it/100?text=NoPoster'} alt="" /> }
            </div>
        );
    }
}

export default MoviePoster;