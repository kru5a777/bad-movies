import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ReactComponent as Logo } from './popcorn.svg';

import MovieItemMore from "./components/movie-item-more/MovieItemMore";
import MovieList from "./components/movie-list/MovieList";

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';

class App extends React.Component {
    render() {
        return (
            <div>
                <header className='container-fluid' >
                    <div>
                        <Logo />
                    </div>
                    <h1>Киновтопку</h1>
                </header>
                <Router>
                    <Route path='/' exact component={MovieList} />
                    <Route path='/more-about-movie/:id' component={(param) => <MovieItemMore needId={param.match.params.id}/>}/>
                </Router>
            </div>
        );
    }
}

export default App;
